package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.button.Button;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.utils.Controller;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {

    //Controllers
    public Controller driverPad;
    public Controller opPad;

    private static final double STICK_DEADBAND = 0.05;

/*
    public Button runClimbtake = new JoystickButton(driverPad, JoystickMap.RB);
    public Button controlledShoot = new JoystickButton(opPad, JoystickMap.LB);
    public Button feed = new JoystickButton(opPad, JoystickMap.RB);
    public Button setPowerVeryLow = new JoystickButton(opPad, JoystickMap.A);
    public Button setPowerLow = new JoystickButton(opPad, JoystickMap.X);
    public Button setPowerMedium = new JoystickButton(opPad, JoystickMap.Y);
    public Button setPowerHigh = new JoystickButton(opPad, JoystickMap.B);
*/

    public OI() {
        this.driverPad = new Controller(0);
        this.opPad = new Controller(1);
/*
        runClimbtake.whileHeld(new ClimbtakeCmd());
        controlledShoot.whileHeld(new VelocityShootCmd());
        feed.whileHeld(new FeedCmd());
        setPowerVeryLow.whenPressed(new ChangeVelocityCmd(Velocities.veryLowPow));
        setPowerLow.whenPressed(new ChangeVelocityCmd(Velocities.lowPow));
        setPowerMedium.whenPressed(new ChangeVelocityCmd(Velocities.mediumPow));
        setPowerHigh.whenPressed(new ChangeVelocityCmd(Velocities.highPow));
*/
    }

    public Controller getDriverController() {
        return this.driverPad;
    }

    public Controller getOperatorController() {
        return this.opPad;
    }
}
