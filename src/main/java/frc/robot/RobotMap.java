package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
    //Intake and Shooter
    public static final int SHOOTER_MOTOR_1 = 2;
    public static final int SHOOTER_MOTOR_2 = 3;
    public static final int CLIMBTAKE_MOTOR_1 = 30;
    public static final int CLIMBTAKE_MOTOR_2 = 13;
    public static final int TOP_HOPPER_MOTOR = 6;
    public static final int SPIN_FEEDER_MOTOR = 4;
    public static final int HOOD_MOTOR_A = 5;

    //Drivetrain
    public static final int LEFT_DRIVE_MAIN = 1;
    public static final int LEFT_DRIVE_FOLLOWER = 20;
    public static final int RIGHT_DRIVE_MAIN = 14;
    public static final int RIGHT_DRIVE_FOLLOWER = 15;

    //Gear Placer
    public static final int WRIST_MOTOR = 19;
    public static final int INTAKE_MOTOR = 23;

    //Servos
    public static final int GEAR_SERVO_RIGHT = 7;
    public static final int GEAR_SERVO_LEFT = 8;

}
